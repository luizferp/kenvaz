package com.luizpinto.android.learning.kenvaz;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;

public class DrawActivity extends Activity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_draw);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.actionbar_actions, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        CanvasView canvas = (CanvasView)findViewById(R.id.canvasView);
        switch (item.getItemId()) {
            case R.id.menu_clear:
            {
                canvas.clearCanvas();
                return true;
            }
            case R.id.menu_color:
            {
                View menuColorView = findViewById(R.id.menu_color);
                PopupMenu popup = new PopupMenu(this, menuColorView);
                popup.setOnMenuItemClickListener(new OnColorMenuItemClickListener(canvas));

                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.colors, popup.getMenu());
                popup.show();
                return true;
            }
            case R.id.menu_pen:

                View menuPenView = findViewById(R.id.menu_pen);
                PopupMenu popup = new PopupMenu(this, menuPenView);
                popup.setOnMenuItemClickListener(new OnPenMenuItemClickListener(canvas));

                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.stroke_width, popup.getMenu());
                popup.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
