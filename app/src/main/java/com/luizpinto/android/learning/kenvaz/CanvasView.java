package com.luizpinto.android.learning.kenvaz;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import java.util.HashMap;
import java.util.Set;

/**
 * Created by luiz.pinto on 25/10/2016.
 */

class StrokeWidth
{
    public static final float MICRO = 2.0f;
    public static final float FINE = 8.0f;
    public static final float MEDIUM = 16.0f;
    public static final float BOLD  = 32.0f;
}

class PaintStyle {
    int color;
    float strokeWidth;

    public int getColor()
    {
        return color;
    }

    public float getStrokeWidth()
    {
        return strokeWidth;
    }

    public void setColor(int color)
    {
        this.color = color;
    }

    public void setStrokeWidth(float strokeWidth)
    {
        this.strokeWidth = strokeWidth;
    }
}

public class CanvasView extends View
{

    private Paint paint;
    private Path canvasPath;
    private float mX, mY;

    private HashMap<Integer, Integer> colorMap;
    private HashMap<Integer, Float> strokeWidthMap;
    private HashMap<Path, PaintStyle> pathToPaintStyleMap;

    public CanvasView(Context context, AttributeSet attrs)
    {
        super(context, attrs);

        colorMap = new HashMap<>();
        strokeWidthMap = new HashMap<>();
        pathToPaintStyleMap = new HashMap<>();

        colorMap.put(R.id.color_black, Color.BLACK);
        colorMap.put(R.id.color_blue, Color.BLUE);
        colorMap.put(R.id.color_green, Color.GREEN);
        colorMap.put(R.id.color_red, Color.RED);
        colorMap.put(R.id.color_yellow, Color.YELLOW);

        strokeWidthMap.put(R.id.width_micro, StrokeWidth.MICRO);
        strokeWidthMap.put(R.id.width_fine, StrokeWidth.FINE);
        strokeWidthMap.put(R.id.width_medium, StrokeWidth.MEDIUM);
        strokeWidthMap.put(R.id.width_bold, StrokeWidth.BOLD);

        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(colorMap.get(R.id.color_black));
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setStrokeWidth(StrokeWidth.FINE);

        canvasPath = new Path();
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);

        int currentColor = paint.getColor();
        float currentStrokeWidth = paint.getStrokeWidth();

        for (HashMap.Entry<Path, PaintStyle> entry: pathToPaintStyleMap.entrySet())
        {
            paint.setColor(entry.getValue().getColor());
            paint.setStrokeWidth(entry.getValue().getStrokeWidth());
            canvas.drawPath(entry.getKey(), paint);
        }

        paint.setColor(currentColor);
        paint.setStrokeWidth(currentStrokeWidth);
        canvas.drawPath(canvasPath, paint);
    }

    protected void clearCanvas()
    {
        Set<Path> pathSet = pathToPaintStyleMap.keySet();
        for (Path path: pathSet)
        {
            path.reset();
        }

        invalidate();
    }

    protected void setPaintColor(int color)
    {
        paint.setColor(colorMap.get(color));
    }

    protected void setPaintStrokeWidth(int width)
    {
        paint.setStrokeWidth(strokeWidthMap.get(width));
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        float x = event.getX();
        float y = event.getY();

        switch (event.getAction())
        {
            case MotionEvent.ACTION_DOWN:
            {
                mX = x;
                mY = y;

                canvasPath = new Path();
                canvasPath.moveTo(mX, mY);
                break;
            }
            case MotionEvent.ACTION_MOVE:
            {
                canvasPath.quadTo(mX, mY, (x + mX)/2, (y + mY) / 2);

                mX = x;
                mY = y;

                invalidate();
                break;
            }
            case MotionEvent.ACTION_UP:
            {
                PaintStyle style = new PaintStyle();
                style.setColor(paint.getColor());
                style.setStrokeWidth(paint.getStrokeWidth());
                pathToPaintStyleMap.put(canvasPath, style);
                break;
            }
        }
        return true;
    }
}
