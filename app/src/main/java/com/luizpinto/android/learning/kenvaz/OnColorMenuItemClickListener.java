package com.luizpinto.android.learning.kenvaz;

import android.view.MenuItem;
import android.widget.PopupMenu;

/**
 * Created by luiz.pinto on 01/11/2016.
 */

public class OnColorMenuItemClickListener implements PopupMenu.OnMenuItemClickListener
{
    CanvasView canvas;

    public OnColorMenuItemClickListener(CanvasView canvas)
    {
        this.canvas = canvas;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item)
    {
        canvas.setPaintColor(item.getItemId());
        return true;
    }
}
