package com.luizpinto.android.learning.kenvaz;

import android.view.MenuItem;
import android.widget.PopupMenu;

/**
 * Created by luiz.pinto on 01/11/2016.
 */
public class OnPenMenuItemClickListener implements PopupMenu.OnMenuItemClickListener
{
    CanvasView canvas;
    public OnPenMenuItemClickListener(CanvasView canvas)
    {
        this.canvas = canvas;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item)
    {
        canvas.setPaintStrokeWidth(item.getItemId());
        return true;
    }
}